if exists('g:loaded_merc') || &cp || !has ('popupwin') || !has ('timers')
  finish
endif
let g:loaded_merc = 1

func s:print (lines) 
	if s:use_popup == 1
		call setbufline (s:popup.bufnr, 1, a:lines)
	else
		echo join (a:lines, ' | ')
	endif
endfunc

func! s:write_report ()
	let report = [
				\ 'merc-vim report of session starting '. strftime ('%c'),
				\ 'points: '. s:score.points, 
				\ 'duration: '. (localtime () - s:time.start + 0),
				\ 'longest run: '. s:score.longest_run,
				\ 'run most points: '. s:score.run_most_points,
				\ 'run most chars: '. s:score.run_most_chars,
				\ ]
	call writefile (report, '/tmp/this')
endfunc

func! Merc_start ()
	" Use something to say this function's been called, so I can return if
	" you call Mercstop without doing Mercstart. It's 0 here; I make it 1 at the
	" end of this function. I don't know if this is clever or not.
	let s:initted = 0

	" It's pretty weird to define the script-local variables in this function.
	" Maybe that's normal in scripting languages. I program with butterflies, so
	" it's new to me.

	let s:use_popup = 1
	if exists ('g:merc_no_popup')
		if g:merc_no_popup == 1
			let s:use_popup = 0
		endif
	endif
	if exists ('g:merc_popup_options')
		let s:popup_options = g:merc_popup_options
	else
		let s:popup_options =  {'line': 1, 'col': 1, 'border': []}
	endif

	let s:score = {'points': 0, 'multiplier': 1, 'run': 0, 'chars_per_point': 5,
				\ 'next_target': 100, 'target_scaler': 1.50, 'longest_run': 0,
				\ 'run_most_points': 0, 'run_most_chars': 0, 'this_run_start_points': 0}
	let s:combo_timeout = 20
	let s:tick = {'timer': 0, 'len': 10}
	if exists ('g:merc_tick_len')
		let s:tick.len = g:merc_tick_len
	else
		" If you're not using the popup, make the tick rate a lot lower so it
		" doesn't spam so much. You can't watch the numbers go  up anyway.
		" Note (feature), for no-popup mode I could just use CursorHold, which
		" seems a bit counter-productive.
		if !s:use_popup
			let s:tick.len = 1000
		endif
	endif
	" The number of chars typed since the last update. If your
	" tick.len is high, this might be more than 1.
	let s:frame_n_chars = 0
	let s:time = {'run_start': -1, 'start': localtime (), 'now': localtime (),
				\ 'last': localtime ()}
	let s:msg_start = 0
	if s:use_popup
		let s:popup = {'winid': 0, 'bufnr': 0}
		let s:popup.winid = popup_create ([], s:popup_options)
		let s:popup.bufnr = winbufnr (s:popup.winid)
	endif
	
	let s:tick.timer = timer_start (s:tick.len, 'Merc_tick',
				\	{'repeat': -1})
	augroup merc
		au!
		au InsertCharPre <buffer> 
					\ let s:frame_n_chars += 1
		" This QuitPre will get called if the buffer closes, obviously.
		" It catches Vim exiting, too. There's no way of stopping you
		" from completely exiting Vim that I know of. Maybe later I'll
		" have something more complicated. Or maybe I'll just have 'reload
		" from report' option.
		au QuitPre <buffer>
					\ call Merc_stop ()
	augroup end
	let s:initted = 1
endfunc
" Don't seem to be able to use s:. Obviously there's a way.
"

func! s:update_report_vars ()
	let s:score.longest_run = max ([s:score.longest_run,
				\ s:time.now - s:time.run_start])
	let s:score.run_most_points = max ([s:score.run_most_points,
				\ s:score.points - s:score.this_run_start_points])
	let s:score.run_most_chars = max ([s:score.run_most_chars,
				\ s:score.run])
endfunc

func! Merc_tick(timer) 
	let s:time.now = localtime ()
	if s:frame_n_chars >= 1
		if s:time.run_start == -1
			let s:this_run_start_points = s:score.points
			let s:time.run_start = s:time.now
		endif
		let s:score.run += s:frame_n_chars
		let s:frame_n_chars = 0
		if s:score.run > s:score.next_target
			let s:score.next_target *= s:score.target_scaler
			let s:score.multiplier += 1
		endif
		if s:score.run % s:score.chars_per_point == 0
			let s:score.points += 1 * s:score.multiplier
		endif
		let s:time.last = s:time.now
	else
		if s:time.now - s:time.last >= s:combo_timeout

			call s:update_report_vars ()
			let s:score.multiplier = 1
			let s:score.run = 0
			let s:time.run_start = -1
			call s:print (['Combo timeout! Score multiplier reset to 1', ''])
			let s:msg_start = s:time.now
		endif
	endif
	if s:msg_start && s:time.now - s:msg_start >= 3
		let s:msg_start = 0
	endif
	" Need to do something like this. s:time.now - s:time.run_start directly
	" in the list seems to produce garbage.
	if s:time.run_start == -1
		let s:run_time = 0
	else
		let s:run_time = s:time.now - s:time.run_start
	endif
	call s:print ([s:time.now - s:time.start . ' ' . s:score.points . 'p',
				\ 
				\ 'run: ' . s:score.run . ', ' . s:run_time . ', *' . s:score.multiplier])
endfunc

func! Merc_stop ()
	if !exists ('s:initted')| return| endif
	augroup merc
		au!
	augroup end
	call s:update_report_vars ()
	call s:write_report ()
	let s:score = 0
	let s:time = 0
	let s:msg_start = 0
	if s:use_popup
		call popup_close (s:popup.winid)
	endif
	call timer_stop (s:tick.timer)
endfunc

command! Mercstop :call Merc_stop ()
command! Mercstart :call Merc_start ()

