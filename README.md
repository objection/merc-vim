# Mercenary Mode Vim

This is a game, of sorts, and hopefully a productivity tool. It's like
Resident evil's Mercenary Mode. In that mode you have to blast zombies
and not let the time run out. You get score multipliers for chains.

This is, right now, a very basic version of that. You get (for now) a
popup window showing your score. If the timer (20) seconds, runs out
before you've typed anything, your score multiplier is reset.

There's no fail state. I think maybe there should be.

Points are got by typing characters. Maybe later I'll do something
more sophisticated. Maybe paragraphs get you more points? Maybe good
grammar does?

